#!/usr/bin/env sh
which helm >/dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "系统中已安装Helm"
  exit 0
fi
curl -sSL https://get.helm.sh/helm-v3.10.1-linux-amd64.tar.gz -o helm.tar.gz
tar -xf helm.tar.gz
mv linux-amd64/helm /usr/local/bin
rm -rf helm.tar.gz linux-amd64